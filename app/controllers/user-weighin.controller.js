/**
 * Created by john.flournoy on 2/12/17.
 */
var mongoose = require('mongoose'),
    User = require('mongoose').model('User'),
    config = require('../config/config.js'),
    UserWeighin = mongoose.model('User-Weighin'),
    Weighin = mongoose.model('Weigh_in');

exports.update = function(req, res){
    UserWeighin.update({
        _id: req.userweighin._id
    }, {
        weigh_in_weight: req.body.weigh_in_weight
    }, function(err, userweighin){
        if(err) res.json(err);
        res.json(userweighin)
    })

}

exports.userWeighinByID = function(req, res, next, id){
    UserWeighin.findById(id, function(err, userweighin){
        if(err) return next(err);
        if(!userweighin) return next(new Error('Failed to load article ' + id));

        req.userweighin = userweighin
        next()
    })
}

exports.hasAuthorization = function(req, res, next){
    // TODO
}