/**
 * Created by john.flournoy on 2/18/17.
 */
var mongoose = require('mongoose'),
    Competition = mongoose.model('Competition');

exports.listByQuery = function(req, res){
    console.log(req.query)
    if(req.query.type == 'Participant'){

        Competition.find().populate({
            path: 'participants',
            select: 'firstName lastName username',
            match: { $or: [
                { firstName: new RegExp(req.query.search, 'i') },
                { lastName: new RegExp(req.query.search, 'i') },
                { username: new RegExp(req.query.search, 'i') }
            ]}
        }).exec(function(err, competitions){
            if(err){
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                competitions = competitions.filter(function(item){
                    return item.participants.length;
                })
                res.json(competitions)
            }
        })

    } else {
        Competition.find({
            title: new RegExp('^' + req.query.search)
        }, function(err, competitions){
            if(err){
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                res.json(competitions)
            }
        })

    }
}