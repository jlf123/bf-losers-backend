/**
 * Created by john.flournoy on 9/3/16.
 */
var User = require('mongoose').model('User'),
    jwt = require('jwt-simple'),
    config = require('../config/config.js'),
    passport = require('passport');


var getErrorMessage = function(err){
    var message;
    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;
            default:
                message = 'Something went wrong';
        }
    } else {
        for (var errName in err.errors) {
            if (err.errors[errName].message) message = err.errors[errName].message;
        }
    }

    return message;
}


exports.signup = function(req, res, next) {
    if (!req.user) {
        var user = new User(req.body);
        var message = null;

        user.provider = 'local';
        user.avatar = 'https://s3.amazonaws.com/johnflournoy/' + user.avatar

        user.save(function(err, user) {
            if (err) {
                var message = getErrorMessage(err);
                res.status(400).send({
                    message: message
                })
            }
            req.login(user, function(err) {
                if (err) return next(err);
            });
            res.status(200).json(user)
        });
    } 
};

exports.signout = function(req, res) {
    req.logout();
};

exports.userByUsername = function(req, res, next){
    User.findOne({
        username : req.user.username
    }, '-password -salt', function(err, user){
        if(err){
            return next(err)
        } else {
            req.user = user
            next();
        }
    })
}

exports.saveOAuthUserProfile = function(req, profile, done){
    User.findOne({
        provider: profile.provider,
        providerId: profile.providerId
    }, function(err, user){
        if(err){
            return done(user)
        } else {
            if(!user){
                var possibleUsername = profile.username || (profile.email ? profile.email.split('@')[0] : '' )
                User.findUniqueUsername(possibleUsername, null, function(availableUsername){
                    profile.username = availableUsername
                    user = new User(profile);
                })
                user.save(function(err) {
                    if (err) {
                        var message = _this.getErrorMessage(err);

                        req.flash('error', message);
                        return res.redirect('/signup');
                    }

                    return done(err, user);
                });
            } else {
                return done(err, user);
            }
        }
    })
}

exports.requiresJWT = function(req, res, next){
    var token = req.headers['flournoyprod-jwt'];
    if(token){
        req.user = jwt.decode(token, config.jwtSecret).user;
        console.log(req.user)
        next();
    } else {
        return res.status(401).send({
            message: 'JWT Required'
        })
    }
}

exports.requiresLogin = function(req, res, next){
    if(!req.isAuthenticated()){
        return res.status(401).send({
            message: 'User is not logged in'
        })
    }

    next();
}

exports.list = function(req, res, next){
    User.find({}, function(err, users){
        if (err) {
            return next(err);
        } else {
            res.json(users);
        }
    })
}




/*exports.create = function(req, res, next){
    var user = new User(req.body);

    user.save(function(err){
        if(err){
            return next(err)
        } else {
            res.json(err);
        }
    })
}



exports.read = function(req, res){
    res.json(req.user);
}

exports.userByID = function(req, res, next, id){

    User.findOne({
        _id : id
    }, function(err, user){
        if(err){
            return next(err)
        } else {
            req.user = user
            next();
        }
    })

}

exports.update = function(req, res, next) {
    User.findByIdAndUpdate(req.user.id, req.body, function(err, user) {
        if (err) {
            return next(err);
        } else {
            res.json(user);
        }
    });
};

exports.delete = function(req, res, next) {
    req.user.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.user);
        }
    })
};
*/
