/**
 * Created by john.flournoy on 8/29/16.
 */


exports.render = function(req, res) {
    res.render('index', {
        title: 'Hello World',
        userFullName: req.user ? req.user.fullName : ''
    });
};
