/**
 * Created by john.flournoy on 2/16/17.
 */
var uuid = require('node-uuid'); // https://github.com/defunctzombie/node-uuid
var multiparty = require('multiparty'); // https://github.com/andrewrk/node-multiparty
var s3 = require('s3'); // https://github.com/andrewrk/node-s3-client

var s3Client = s3.createClient({
    s3Options: {
        accessKeyId: 'AKIAJXCEWBELE3D73ESQ',
        secretAccessKey: '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'
    }
});

exports.uploadimage = function(req, res){
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) {
        console.log(files)
        var file = files.file[0];
        var contentType = file.headers['content-type'];
        var extension = file.path.substring(file.path.lastIndexOf('.'));
        var destPath = 'profile' + '/' + uuid.v4() + extension;

        var headers = {
            'x-amz-acl': 'public-read',
            'Content-Length': file.size,
            'Content-Type': contentType
        };
        var params = {
            localFile: file.path,

            s3Params: {
                Bucket: "johnflournoy",
                Key: destPath
            }
        }

        var uploader = s3Client.uploadFile(params);

        uploader.on('error', function(err) {
            res.status(500).json(err)
            console.log(err)
            //TODO handle this
        });

        uploader.on('end', function(url) {
            //TODO do something with the url
            res.status(200).json(destPath)
        });
    });
}
