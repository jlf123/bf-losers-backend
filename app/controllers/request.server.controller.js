/**
 * Created by john.flournoy on 2/17/17.
 */
var mongoose = require('mongoose'),
    jwt = require('jwt-simple'),
    config = require('../config/config.js'),
    Request = mongoose.model('Request'),
    Competition = mongoose.model('Competition');

var getErrorMessage = function(err){
    if(err.errors){
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }
}

exports.create = function(req, res){
    var request = new Request(req.body);
    request.requester = req.user._id;

    request.save(function(err){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(request)
        }
    })
}

exports.notAParticipantOf = function(req, res, next){
    Competition.findOne({
        _id: req.body.competition
    }, function(err, response){
        console.log(response)
        for(var i = 0; i < response.participants.length; i++){
            if(response.participants[i] == req.user._id){
                res.status(200).json({
                    error: 'You are already a participant in this competition'
                })
            }
        }
        next()
    })

}

exports.notAlreadySent = function(req, res, next){
    Request.find({
        requester: req.user._id,
        competition: req.body.competition
    }, function(err, response){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        }
        if(response.length){
            res.status(200).json({
                error: 'You have already requested to join this competition'
            })
        }
        next()
    })

}

exports.list = function(req, res){
    Request.find({
        decider: req.user._id,
        decision: 'PENDING'
    })
    .populate('requester competition')
    .exec(function(err, data){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            })
        } else {
            res.status(200).json(data)
        }
    })
}

exports.update = function(req, res){
    Request.update({
        _id: req.request._id
    }, {
        decision: req.body.approve ? "APPROVED" : "DECLINED"
    }, {}, function(err, request, data){
        if(err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        }

        if(req.body.approve){
            Competition.update({ _id: req.request.competition },
                { $push: { participants: req.request.requester }}, {},
                function(err){
                    if(err){
                        return res.status(400).send({
                            message: getErrorMessage(err)
                        });
                    } else {
                        res.json(request)
                    }
            })
        }
    })


}

exports.requestById = function(req, res, next, id){
    Request.findById({ _id: id }, function(err, request){
        if(err) return next(err);
        if(!request) return next(new Error('Failed to load article ' + id));

        req.request = request
        next()
    })
}

exports.hasAuthorization = function(req, res, next){
    if(req.request.decider != req.user._id){
        return res.status(403).send({
            message: 'User is not authorized'
        })
    }
    next();
}

exports.requiresJWT = function(req, res, next){
    var token = req.headers['flournoyprod-jwt'];
    if(token){
        req.user = jwt.decode(token, config.jwtSecret).user;
        next();
    } else {
        return res.status(401).send({
            message: 'JWT Required'
        })
    }
}