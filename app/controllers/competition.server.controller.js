/**
 * Created by john.flournoy on 9/7/16.
 */

var mongoose = require('mongoose'),
    jwt = require('jwt-simple'),
    User = require('mongoose').model('User'),
    config = require('../config/config.js'),
    Weighin = mongoose.model('Weigh_in'),
    UserWeighin = mongoose.model('User-Weighin'),
    deepPopulate = require('mongoose-deep-populate')(mongoose),
    Competition = mongoose.model('Competition');

var getErrorMessage = function(err){
    if(err.errors){
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }
}

exports.create = function(req, res){
    var competition = new Competition(req.body);
    competition.creator = req.user._id;

    competition.save(function(err){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(competition)
        }
    })

}

exports.list = function(req, res){
    Competition.find({ $or: [
        {creator: req.user._id},
        { participants: req.user._id}]
    }).sort('-created')
        .populate('creator')
        .populate('participants')
        .exec(function(err, compeitions){
            if(err){
                return res.status(400).send({
                    message: getErrorMessage(err)
                })
            } else {
                res.status(200).json(compeitions)
            }
    })
}

exports.update = function(req, res){
    var competition = req.competition;
    competition.title = req.body.title;

    competition.save(function(err){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            })
        } else {
            res.json(competition)
        }
    })
}

exports.requiresJWT = function(req, res, next){
    var token = req.headers['flournoyprod-jwt'];
    if(token){
        req.user = jwt.decode(token, config.jwtSecret).user;
        next();
    } else {
        return res.status(401).send({
            message: 'JWT Required'
        })
    }
}

exports.delete = function(req, res){
    var competition = req.competition;

    competition.remove(function(err){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            })
        } else {
            res.json(competition)
        }
    })
}

exports.competitionByID = function(req, res, next, id){
    Competition.findById(id).populate('creator', 'firstName lastName fullName').exec(function(err, competition){
        if(err) return next(err);
        if(!competition) return next(new Error('Failed to load article ' + id));

        req.competition = competition
        next()
    })
}

exports.hasAuthorization = function(req, res, next){
    if(req.competition.creator._id != req.user._id){
        return res.status(403).send({
            message: 'User is not authorized'
        })
    }
    next();
}

exports.read = function(req, res){
    Competition.deepPopulate(req.competition, 'weigh_ins.individual_weighins.participant', function(err, _competition){
        res.json(_competition)
    })
}

