/**
 * Created by john.flournoy on 2/9/17.
 */
var mongoose = require('mongoose'),
    jwt = require('jwt-simple'),
    User = require('mongoose').model('User'),
    config = require('../config/config.js'),
    Competition = mongoose.model('Competition'),
    UserWeighin = mongoose.model('User-Weighin'),
    Weighin = mongoose.model('Weigh_in');

var getErrorMessage = function(err){
    if(err.errors){
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }
}

exports.create = function(req, res){
    var userweighins = [];
    var tmpweighin;

    for(var i = 0; i < req.body.user_weighins.length; i++){
        tmpweighin = new UserWeighin(req.body.user_weighins[i]);
        tmpweighin.save();
        userweighins.push(tmpweighin._id)
    }

    var weighin = new Weighin({
        individual_weighins: userweighins
    })

    weighin.save(function(err){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            Competition.update({ _id: req.competition._id},
                { $push: { weigh_ins: weighin._id }}, {},
                    function(err, data){
                        if(err){
                            return res.status(400).send({
                                message: getErrorMessage(err)
                            });
                        } else {
                            res.json(weighin)
                        }
                })
        }
    })
}

exports.putAuthorization = function(req, res){
    //TODO
}

exports.weighinByID = function(req, res, next, _id){
    Weighin.findById(_id).populate('individual_weighins').exec(function(err, weighin){
        if(err) return next(err);
        if(!weighin) return next(new Error('Failed to load article ' + id));

        req.weighin = weighin
        next()
    })
}

exports.list = function(req, res){
    //TODO
}

exports.update = function(req, res){
    var userweighin =  new UserWeighin(req.body);

    userweighin.save(function(err, data){
        Weighin.update({_id: req.weighin._id},
            { $push: { individual_weighins: userweighin }}, {},
            function(err, data){
                if(err){
                    return res.status(400).send({
                        message: getErrorMessage(err)
                    });
                } else {
                    res.json(data)
                }
            })

    })
}

exports.requiresJWT = function(req, res, next){
    var token = req.headers['flournoyprod-jwt'];
    if(token){
        req.user = jwt.decode(token, config.jwtSecret).user;
        next();
    } else {
        return res.status(401).send({
            message: 'JWT Required'
        })
    }
}

exports.delete = function(req, res){
    var weighin = req.weighin;

    weighin.remove(function(err){
        if(err){
            return res.status(400).send({
                message: getErrorMessage(err)
            })
        } else {
            res.json(weighin)
        }
    })
}

exports.competitionByID = function(req, res, next, id){
    Competition.findById(id).populate('creator', 'firstName lastName fullName').exec(function(err, competition){
        if(err) return next(err);
        if(!competition) return next(new Error('Failed to load article ' + id));

        req.competition = competition
        next()
    })
}

exports.hasAuthorization = function(req, res, next){
    if(req.competition.creator.id !== req.user.id){
        return res.status(403).send({
            message: 'User is not authorized'
        })
    }
    next();
}