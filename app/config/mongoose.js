var config = require('./config'),
    mongoose = require('mongoose');

module.exports = function(){
    var db = mongoose.connect(config.db);

    require('../models/user.server.model.js');
    require('../models/competition.server.model.js')
    require('../models/weighin.server.model')
    require('../models/user-weighin.server.model')
    require('../models/request.server.model')

    return db;
}