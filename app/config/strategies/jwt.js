var passport = require('passport'),
    passportJWT = require('passport-jwt'),
    config = require('../config'),
    User = require('mongoose').model('User'),
    extractJwt = passportJWT.ExtractJwt,
    JWTStrategy = passportJWT.Strategy,
    params = {
        secretOrKey: config.jwtSecret,
        jwtFromRequest:extractJwt.fromAuthHeader()
    }

module.exports = function(){

    passport.use(new JWTStrategy(params, function(username, password, done) {
        User.findOne(username, function(err, user) {
            if (err) { return done(err, false); }

            if (!user) {
                done(null, false, {
                    message: 'Unknown User'
                });
            }

            if (!user.authenticate(password)) {
                return done(null, false, {
                    message: 'Invalid password'
                });
            }
            return done(null, user);
        });
    }));

}
