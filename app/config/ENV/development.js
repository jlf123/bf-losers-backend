/**
 * Created by john.flournoy on 8/31/16.
 */

module.exports = {
    db: 'mongodb://database_joe:superman15@ds023245.mlab.com:23245/losers',
    sessionSecret: 'developmentSessionSecret',
    facebook: {
        clientID : '177562399341073',
        clientSecret : '469e9940ac0014f47b16a065db79c96b',
        callbackURL: 'http://localhost:3000/oauth/facebook/callback'
    },
    jwtSecret: 'ILoveMelissa',
    jwtSession: {
        session: false
    }
}
