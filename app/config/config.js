/**
 * Created by john.flournoy on 8/31/16.
 */

module.exports = require('./ENV/' + process.env.NODE_ENV + '.js');

//this will either load the developmenet config or the production config depending on which env you are working on
// One good example of this......when connecting to mongoDB you would have a different connection string for dev vs production
// so being able to specifiy which file to use will make loading shit a lot easier
