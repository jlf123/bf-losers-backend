/**
 * Created by john.flournoy on 2/18/17.
 */
var query = require('../controllers/query.server.controller')

module.exports = function(app){
    app.route('/losers/Query')
        .get(query.listByQuery)
}