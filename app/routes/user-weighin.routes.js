/**
 * Created by john.flournoy on 2/12/17.
 */

var weighin = require('../controllers/weighin.server.controller.js'),
    user_weighin = require('../controllers/user-weighin.controller');

module.exports = function(app){

    app.route('/losers/IndividualWeighin/:indvWeighinId')
        .put(user_weighin.update)


    app.param('indvWeighinId', user_weighin.userWeighinByID);
}
