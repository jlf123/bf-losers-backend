/**
 * Created by john.flournoy on 2/17/17.
 */
var request = require('../../app/controllers/request.server.controller.js'),
    competitions = require('../../app/controllers/competition.server.controller');

module.exports = function(app){

    app.route('/losers/Request')
        .get(request.requiresJWT, request.list)
        .post(request.requiresJWT, request.notAParticipantOf, request.notAlreadySent, request.create)

    app.route('/losers/Request/:requestId')
        .put(request.requiresJWT, request.hasAuthorization, request.update);

    app.param('requestId', request.requestById)
}