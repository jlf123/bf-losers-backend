/**
 * Created by john.flournoy on 9/3/16.
 */
var user = require('../../app/controllers/user.server.controller.js'),
    request = require('request'),
    jwt = require('jwt-simple'),
    config = require('../config/config.js'),
    passport = require('passport');

module.exports = function(app){

    app.route('/Profile')
        .post(user.signup)
        .get(user.requiresJWT, user.userByUsername, function(req, res){
            res.status(200).json(req.user);
        })

    app.route('/Sessions')
        .post(passport.authenticate('local'), function(req, res){
            if(req.user){
                var payload = {
                    user:{
                        username: req.user.username,
                        _id: req.user._id
                    }
                }
                var token = jwt.encode(payload, config.jwtSecret)
                res.status(200).send({
                    token: token
                });
            }
        })

    app.route('/Users')
        .get(user.requiresJWT, user.list)


    app.get('/oauth/facebook', passport.authenticate('facebook', {
        failureRedirect: '/signin'
    }));
    app.get('/oauth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/signin',
        successRedirect: '/'
    }));

    app.get('/signout', user.signout);


}