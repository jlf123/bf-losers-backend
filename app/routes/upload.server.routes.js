/**
 * Created by john.flournoy on 2/15/17.
 */
var upload = require('../../app/controllers/upload.server.controller.js');

module.exports = function(app){

    app.route('/losers/upload/image')
        .post(upload.uploadimage)
}