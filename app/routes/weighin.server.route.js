/**
 * Created by john.flournoy on 2/9/17.
 */
/**
 * Created by john.flournoy on 9/7/16.
 */

var weighin = require('../controllers/weighin.server.controller.js'),
    competitions = require('../controllers/competition.server.controller.js');

module.exports = function(app){

    app.route('/losers/Competition/:competitionId/Weighin')
        .get(competitions.requiresJWT, weighin.list)
        .post(competitions.requiresJWT, competitions.hasAuthorization, weighin.create)

    app.route('/losers/Competition/:competitionId/Weighin/:weighinId')
        .get(competitions.read)
        .put(competitions.requiresJWT, weighin.update)
        .delete(competitions.requiresJWT, competitions.hasAuthorization, weighin.delete)

    app.param('weighinId', weighin.weighinByID);
    app.param('competitionId', competitions.competitionByID)
}