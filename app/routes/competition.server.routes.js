/**
 * Created by john.flournoy on 9/7/16.
 */

var users = require('../controllers/user.server.controller.js'),
    competitions = require('../controllers/competition.server.controller.js');

module.exports = function(app){

    app.route('/losers/Competitions')
        .get(competitions.requiresJWT, competitions.list)
        .post(competitions.requiresJWT, competitions.create)

    app.route('/losers/Competitions/:competitionId')
        .get(competitions.requiresJWT, competitions.read)
        .put(users.requiresLogin, competitions.hasAuthorization, competitions.update)
        .delete(users.requiresLogin, competitions.hasAuthorization, competitions.delete)

    app.param('competitionId', competitions.competitionByID);


}