/**
 * Created by john.flournoy on 2/16/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RequestSchema = new Schema({
    decider: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    requester: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    competition: {
        type: Schema.ObjectId,
        ref: 'Competition'
    },
    decision: {
        type: String,
        default: 'PENDING'
    }

})

mongoose.model('Request', RequestSchema);