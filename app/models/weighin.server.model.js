var mongoose = require('mongoose'),
    deepPopulate = require('mongoose-deep-populate')(mongoose),
    Schema = mongoose.Schema;

var WeighinSchema = new Schema({
    weigh_in_date: {
        type: Date,
        default: Date.now
    },
    individual_weighins: [
        {
            type: Schema.ObjectId,
            ref: 'User-Weighin'
        }
    ]
})

WeighinSchema.plugin(deepPopulate);

mongoose.model('Weigh_in', WeighinSchema);