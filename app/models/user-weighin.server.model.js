var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserWeighinSchema = new Schema({
    participant: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    weigh_in_weight: Number
})

mongoose.model('User-Weighin', UserWeighinSchema);