/**
 * Created by john.flournoy on 9/7/16.
 */
var mongoose = require('mongoose'),
    deepPopulate = require('mongoose-deep-populate')(mongoose),
    Schema = mongoose.Schema;

var CompetitionSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    end_date: Date,
    title: {
        type: String,
        trim: true,
        required: 'Title cannot be blank'
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    participants: [
        {
            type: Schema.ObjectId,
            ref: 'User',
            required: 'You need at least 2 participants to start a competition'
        }
    ],
    weigh_ins: [
        {
            type: Schema.ObjectId,
            ref: 'Weigh_in'
        }
    ]
    //Add a weigh-in object here that will contain all the weigh-in's
})

CompetitionSchema.plugin(deepPopulate);

mongoose.model('Competition', CompetitionSchema);

